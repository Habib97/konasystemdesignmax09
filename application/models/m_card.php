<?php

class M_card extends CI_Model {

    function addCard($data)
    {
        $this->load->database();
        
        if($this->db->insert('card_info',$data)) 
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    function deleteCard($data)
    {
        $this->load->database();
        
        if($this->db->delete('card_info',$data)) 
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
}

