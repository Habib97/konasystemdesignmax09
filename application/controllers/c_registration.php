<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_registration extends CI_Controller 
{
    public function signin()
    {
        $this->load->view('v_registration');
    }

    public function registration()
    {
            $this->load->library('form_validation');
            if($_POST)
            {
                $config = array(
                                array(
                                        'field' => 'name',
                                        'rules' => 'required|trim|min_length[4]|max_length[30]'
                                ),
                                array(
                                        'field' => 'phone',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'address',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'balance',
                                        'rules' => 'required|trim'
                                )
                        );
                $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
		{
                    $data['success'] = 0;
                    $data['msg'] = "form validation fail.";
                    $this->load->view('v_result',$data);
                    return;
		}
                
                
                
                $this->load->model('m_customer');

                $data = array(
                            'customer_name' => $this->input->post('name',true),
                            'phone_no' => $this->input->post('phone',true),
                            'address' => $this->input->post('address',true),
                            'account_balance' => $this->input->post('balance',true)       
                         );
                
                

                if($this->m_customer->addCustomer($data))
                {
                    $data['success'] = 1;
                    $data['msg'] = "add customer .success";
                    $this->load->view('v_result',$data);
                }
                else
                {
                    $data['success'] = 0;
                    $data['msg'] = "add user.fail";
                    $this->load->view('v_result',$data);
                }
            }

    }
    
    public function login()
    {
        $this->load->view('v_login');
    }
    
    public function checkLogin()
    {
        $this->load->library('form_validation');
            
            if($_POST)
            {
                $config = array(
                                array(
                                        'field' => 'email',
                                        'rules' => 'required|trim|min_length[4]|max_length[30]'
                                ),
                                array(
                                        'field' => 'password',
                                        'rules' => 'required|trim'
                                )
                        );
                $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
		{
                    $data['success'] = 0;
                    $data['msg'] = "Form validation fail.";
                    $this->load->view('v_result',$data);
                    return;
		}
                
                
                
                

                $data = array(
                            'email' => $this->input->post('email',true),
                            'password' => $this->input->post('password',true)
                         );
                
                $this->load->helper('email');
                
                if (!valid_email($data['email']))
                {
                    $data['success'] = 0;
                    $data['msg'] = "Email check success. (fail)";
                    $this->load->view('v_result',$data);
                    return;
                }
                
                $this->load->model('m_registration');
                $user = $this->m_registration->checkUser($data);
                if($user)
                {
                    $data['success'] = 1;
                    $data['msg'] = "login user. (success) ".$user['email'];
                    $this->load->view('v_result',$data);
                }
                else
                {
                    $data['success'] = 0;
                    $data['msg'] = "login user. (fail)";
                    $this->load->view('v_result',$data);
                }
            }
    }
}

