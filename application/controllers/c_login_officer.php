<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_login_officer extends CI_Controller {
    
    
    public function loginOfficer(){
        $this->load->view('v_login_officer');
    }
    
    public function logoutOfficer()
    {	
        $this->load->library('session');
	$sess_array = $this->session->all_userdata();

        foreach($sess_array as $key =>$val){

           if($key!='session_id'||$key!='last_activity'||$key!='admin_id'){
                $this->session->unset_userdata($key);
            }

          }
          
          redirect(base_url().'index.php/c_home/vhome');
    }
    
    public function checkOfficer()
    {
        $this->load->library('form_validation');
        $this->load->library('session');
            
            if($_POST)
            {
                $config = array(
                                array(
                                        'field' => 'username',
                                        'rules' => 'required|trim|min_length[4]|max_length[30]'
                                ),
                                array(
                                        'field' => 'password',
                                        'rules' => 'required|trim'
                                )
                        );
                $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
		{
                    $data['success'] = 0;
                    $data['msg'] = "Form validation fail.";
                    $this->load->view('v_result',$data);
                    return;
		}
                
                
                
                

                $data = array(
                            'username' => $this->input->post('username',true),
                            'password' => $this->input->post('password',true)
                         );
                
                /*$this->load->helper('email');
                
                if (!valid_email($data['email']))
                {
                    $data['success'] = 0;
                    $data['msg'] = "Email check success. (fail)";
                    $this->load->view('v_result',$data);
                    return;
                }*/
                
                $this->load->model('m_login_officer');
                $user = $this->m_login_officer->checkUser($data);
                if($user)
                {
                    $data['success'] = 1;
                    $data['msg'] = "Welcome ".": ".$user['username'];
                    $this->session->set_userdata('officer',$user);
                    $this->load->view('v_officer_home',$data);
                }
                else
                {
                    $data['success'] = 0;
                    $data['msg'] = "login user. (fail)";
                    $this->load->view('v_result',$data);
                }
            }
    }
}

