<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_customer extends CI_Controller {
    function addCustomer()
    {
        $this->load->view('v_add_customer');
    }
    function editCustomer()
    {
        $this->load->view('v_edit_customer');
    }
    public function addCustomerDB()
    {
            $this->load->library('form_validation');
            if($_POST)
            {
                $config = array(
                                array(
                                        'field' => 'name',
                                        'rules' => 'required|trim|min_length[4]|max_length[30]'
                                ),
                                array(
                                        'field' => 'phone',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'address',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'balance',
                                        'rules' => 'required|trim'
                                )
                        );
                $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
		{
                    $data['success'] = 0;
                    $data['msg'] = "form validation fail.";
                    $this->load->view('v_result',$data);
                    return;
		}
                
                
                
                $this->load->model('m_customer');

                $data = array(
                            'customer_name' => $this->input->post('name',true),
                            'phone_no' => $this->input->post('phone',true),
                            'address' => $this->input->post('address',true),
                            'account_balance' => $this->input->post('balance',true)       
                         );
                
                

                if($this->m_customer->addCustomer($data))
                {
                    $data['success'] = 1;
                    $data['msg'] = "add customer .success";
                    $this->load->view('v_result',$data);
                }
                else
                {
                    $data['success'] = 0;
                    $data['msg'] = "add user.fail";
                    $this->load->view('v_result',$data);
                }
            }

    }
}

    
