<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_card extends CI_Controller {
    function addCard()
    {
        $this->load->view('v_add_card');
    }
    function editCard()
    {
        $this->load->view('v_edit_card');
    }
    public function addCardDB()
    {
            $this->load->library('form_validation');
            if($_POST)
            {
                $config = array(
                                array(
                                        'field' => 'cardissuer',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'cardtype',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'name',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'cid',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'pan',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'expdate',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'cvc',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'creditlimit',
                                        'rules' => 'required|trim'
                                )
                        );
                $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
		{
                    $data['success'] = 0;
                    $data['msg'] = "form validation fail.";
                    $this->load->view('v_result',$data);
                    return;
		}
                
                
                
                $this->load->model('m_card');

                $data = array(
                            'customer_id' => $this->input->post('cid',true),
                            'holder_name' => $this->input->post('name',true),
                            'card_issuer' => $this->input->post('cardissuer',true),
                            'card_type' => $this->input->post('cardtype',true),
                            'pan' => $this->input->post('pan',true),
                            'expiry_date' => $this->input->post('expdate',true),
                            'cvc' => $this->input->post('cvc',true),
                            'credit_limit' => $this->input->post('creditlimit',true)
                         );
                
                

                if($this->m_card->addCard($data))
                {
                    $data['success'] = 1;
                    $data['msg'] = "add card .success";
                    $this->load->view('v_result',$data);
                }
                else
                {
                    $data['success'] = 0;
                    $data['msg'] = "add card.fail";
                    $this->load->view('v_result',$data);
                }
            }

    }
     public function deleteCardDB()
     {
         $this->load->library('form_validation');
            if($_POST)
            {
                $config = array(
                                array(
                                        'field' => 'name',
                                        'rules' => 'required|trim'
                                ),
                                array(
                                        'field' => 'id',
                                        'rules' => 'required|trim'
                                )
                                
                     );
                $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
		{
                    $data['success'] = 0;
                    $data['msg'] = "form validation fail.";
                    $this->load->view('v_result',$data);
                    return;
		}
                
                
                
                $this->load->model('m_card');
                 $data = array(
                            'customer_id' => $this->input->post('id',true),
                            'holder_name' => $this->input->post('name',true)
                            
                         );
                 
                  if($this->m_card->deleteCard($data))
                {
                    $data['success'] = 1;
                    $data['msg'] = "add card .success";
                    $this->load->view('v_result',$data);
                }
                else
                {
                    $data['success'] = 0;
                    $data['msg'] = "add card.fail";
                    $this->load->view('v_result',$data);
                }
         
         
     }
     
}

}
